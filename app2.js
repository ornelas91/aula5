const express = require('express');
const app = express();
const PORT = 8000;

app.use(
    express.urlencoded({extended:true})
);

app.get('/usuario', (req, res) => {
    let id = req.query.id;
    let nome = req.query.nome;
    let sobrenome = req.query.sobrenome;

    if(id<0){
        res.status(404).send("Id inválido");
    }else{
        res.status(200).send("Method GET - /usuario - " + nome);
    }

});

app.get("/usuario/:id/:nome/:sobrenome", (req,res)=>{
    let id = req.params.id;
    let nome = req.params.nome;
    let sobrenome = req.params.sobrenome;
    console.log(req);
    res.status(200).send("Method GET (Param) - /usuario " + nome);
});

app.get('/usuario/form', (req, res)=>{
    console.log(req);
    res.status(200).send("Method GET (BODY) - /usuario ");
});

app.post('/usuario', (req, res) => {
    let id = req.query.id;
    let nome = req.query.nome;
    let sobrenome = req.query.sobrenome;
    res.send("Method POST - /usuario - id: " + id + " - nome: " + nome);
});

app.put('/usuario', (req, res) => {
    res.send("Method PUT - /usuario");
});

app.delete('/usuario', (req, res) => {
    res.send("Method DELETE - /usuario");
});

// crud = create read update delete

app.get('/cliente', (req, res) => {
    res.send("Method GET - /cliente");
    console.log("Method GET - /cliente");
});

app.post('/cliente', (req, res) => {
    res.send("Method GET - /cliente");
    console.log("Method GET - /cliente");
});

app.listen(PORT,() => {
    console.log('Projeto iniciado na porta ${PORT}');
});