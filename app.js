const express = require('express');
const app     = express();
const cors = require('cors');
app.use(cors());

app.get('/',(req, res)=>{
    console.log("CHAMANDO VIA GET!");
    res.send("Method GET - Route: /");
});

app.get('/usuario',(req, res)=>{
    console.log("CHAMANDO VIA GET - consulta usuário!");
    res.send("Method GET - Route: /usuario");
});

app.get('/cliente',(req, res)=>{
    console.log("CHAMANDO VIA GET - consulta cliente!");
    console.log(req.query);
    let nome = req.query.fname;
    let sobrenome = req.query.lname;
    let retorno;
    retorno = "<h1>Nome:</h1>" + nome + "<br/>";
    retorno += "<h1>Sobrenome:</h1>" + sobrenome;
    res.send(retorno);
});

app.post('/cliente',(req, res)=>{
    console.log("Method POST - Route: /cliente");
    res.send("Method POST utilizado");
});

app.delete('/cliente',(req, res)=>{
    console.log("Method DELETE - Route: /cliente");
    res.send("Method DELETE utilizado");
});

app.put('/cliente',(req, res)=>{
    console.log("Method PUT - Route: /cliente");
    res.send("Method PUT utilizado");
});

app.listen(8000, function (){
    console.log("Projeto iniciado na porta 8000");
});